#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Modules
import numpy as np
import matplotlib.pyplot as plt
import datetime, uuid, sys, random

# Global parameters
width = 6000 # Figure size [pixels]
height = 6000 # Figure size [pixels]
n_points = 1e6 # Number of random points to iterate
zoom = 1 # Zoom factor, greater than 1 (1 = no zoom)
n_max_iteration = 5000 # Maximum number of iteration of Zn

red_range = n_max_iteration+1
green_range = int(n_max_iteration/10)
blue_range = int(n_max_iteration/100)

settings = {
    "width": width,
    "height": height,
    "zoom": zoom,
    "n_max_iteration": n_max_iteration,
    "n_points": n_points
    }

# Functions
def compute_buddhabrot(settings):
    x_array = np.linspace(-2, 1, settings['width'])/settings['zoom']
    y_array = np.linspace(-1.5, 1.5, settings['height'])/settings['zoom']
    x_min, x_max, x_size = x_array.min(), x_array.max(), settings['width']
    y_min, y_max, y_size = y_array.min(), y_array.max(), settings['height']
    
    coeff = get_nearest_function_coefficient(x_min, x_max, x_size, y_min, y_max, y_size)
    
    t_array_buddhabrot_red = np.zeros((len(y_array), len(x_array)))
    t_array_buddhabrot_green = np.zeros((len(y_array), len(x_array)))
    t_array_buddhabrot_blue = np.zeros((len(y_array), len(x_array)))
    t_array_buddhabrot_rgb = np.zeros((len(y_array), len(x_array), 3)) 

    for ii in range(int(settings["n_points"])):
        #progress(int(100*ii/settings["n_points"]))
        c = random.uniform(-2, 1) + 1j*random.uniform(0, 1.5)
        
        # Check Mandelbrot known points
        p = np.sqrt((c.real-0.25)**2+c.imag**2)
        if c.real < p -2*p**2+0.25 or (c.real+1)**2+c.imag**2 < 0.0625:
            continue
        
        value_nearest_array = []
        has_escaped = [False, False, False]
        z = 0
        z_abs = 0
        for idx_iteration in range(0, settings['n_max_iteration']):
            try:
                z = z**2+c
                z_abs = np.abs(z)
                if z_abs > 2:
                    has_escaped[0] = has_escaped[0] or idx_iteration < red_range
                    has_escaped[1] = has_escaped[1] or idx_iteration < green_range
                    has_escaped[2] = has_escaped[2] or idx_iteration < blue_range
                if z_abs > 1e9:
                    break
                if z_abs < 10 and z.real >= x_min and z.real <= x_max and z.imag >= y_min and z.imag <= y_max:
                    value_nearest_array.append(z)
            except OverflowError as err:
                break
        if has_escaped is not [False, False, False]:
            for idx_value, value in enumerate(value_nearest_array):   
                idx_nearest = get_nearest_idx(value, coeff)
                idx_nearest_sym = (y_size-1 - idx_nearest[0], idx_nearest[1])
                      
                if has_escaped[0]:
                    t_array_buddhabrot_red[idx_nearest] += 1
                    t_array_buddhabrot_red[idx_nearest_sym] += 1
                if has_escaped[1]:
                    t_array_buddhabrot_green[idx_nearest] += 1
                    t_array_buddhabrot_green[idx_nearest_sym] += 1
                if has_escaped[2]:
                    t_array_buddhabrot_blue[idx_nearest] += 1 
                    t_array_buddhabrot_blue[idx_nearest_sym] += 1

    t_array_buddhabrot_rgb[:,:,0] = t_array_buddhabrot_red
    t_array_buddhabrot_rgb[:,:,1] = t_array_buddhabrot_green
    t_array_buddhabrot_rgb[:,:,2] = t_array_buddhabrot_blue
    
    sys.stdout.write('\r')
    sys.stdout.flush()
    return x_array, y_array, t_array_buddhabrot_rgb

def progress(percent=0, width=40):
    left = width * percent // 100
    right = width - left
    
    tags = "#" * left
    spaces = " " * right
    percents = f"{percent:.0f}%"
    
    print("\n[", tags, spaces, "]", percents, sep="", end="")

def get_nearest_function_coefficient(x_min, x_max, x_size, y_min, y_max, y_size):
    return [y_max/(y_max-y_min)*y_size, -1/(y_max-y_min)*y_size, x_max/(x_max-x_min)*x_size, -1/(x_max-x_min)*x_size]

def get_nearest_idx(value, coeff):
    return (int(coeff[0] + value.imag*coeff[1]), int(coeff[2] + value.real*coeff[3]))

def apply_sqrt_color(t_array, gamma=2, max_value=-1):
    if t_array.max() == 0:
        return t_array
    if max_value == -1:
        max_value = t_array.max()
    
    t_array_new = t_array / max_value
    t_array_new = np.power(t_array_new, 1/gamma)
    return t_array_new

def plot_buddhabrot(x_array, y_array, t_array, settings):
    t_array_plot = t_array.copy()
    for ii in range(t_array_plot.shape[2]):
        t_array_plot[:,:,ii] = apply_sqrt_color(t_array_plot[:,:,ii], max_value=t_array_plot.max())
    else:
        t_array_plot = apply_sqrt_color(t_array_plot)
        
    fig, ax = plt.subplots()
    fig.set_size_inches(6, 6)
    ax.imshow(t_array_plot, interpolation=None)
    ax.set_title("Buddhabrot Set")
    plt.xlabel("Real axis")
    plt.ylabel("Imaginary axis")
    plt.show()
    
    return fig, ax

def save_figure(fig, folder, dpi):
    filename = "buddhabrot_{}.png".format(uuid.uuid4())
    filepath = "{}/{}".format(folder, filename)
    fig.savefig(filepath, format="png", dpi=dpi)
    return filepath

print('Start of calculation')
t_start = datetime.datetime.now()

x_array, y_array, t_array = compute_buddhabrot(settings)
    
t_end = datetime.datetime.now()
print('Completed !')
print("Calculation duration: {} seconds".format(t_end-t_start))

print('Plot Buddhabrot figure')
fig, ax = plot_buddhabrot(x_array, y_array, t_array, settings)

# Save image to /output
filepath = save_figure(fig=fig, folder="./output", dpi=len(x_array)/6)
print('Save figure to {}'.format(filepath))
