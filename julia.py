#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Modules
import numpy as np
import matplotlib.pyplot as plt
import datetime, uuid

# Global parameters
center = 0 # Center of the figure [complex]
width = 5000 # Figure size width [pixels]
height = 5000 # Figure size height [pixels]
zoom = 1 # Zoom factor, greater than 1 (1 = no zoom)
n_max_iteration = 5000 # Maximum number of iteration of Zn
c_julia = 0.285-0.01*1j

settings = {
    "center": center,
    "width": width,
    "height": height,
    "zoom": zoom,
    "n_max_iteration": n_max_iteration,
    'c_julia': c_julia
    }

# Functions
def compute_julia(settings):
    x_array = np.linspace(-1.5, 1.5, settings['width'])/settings['zoom'] + settings['center'].real
    y_array = np.linspace(-1.5, 1.5, settings['height'])/settings['zoom'] + settings['center'].imag
    
    xx, yy = np.meshgrid(x_array, y_array)
    m_array = xx + 1j*yy
    t_array = np.zeros((len(y_array), len(x_array)), dtype = 'float64')
    index_compute = np.indices(m_array.shape)
    idx_array = list(zip(index_compute[0].flatten(), index_compute[1].flatten()))      
    for idx_iteration in range(1, settings['n_max_iteration']):
        idx_array_new = []
        for ii, idx in enumerate(idx_array):
            m_array[idx] = m_array[idx]**2+settings['c_julia']
            if np.abs(m_array[idx]) > 1e3:
                t_array[idx] = idx_iteration + 1
                continue
            idx_array_new.append(idx)
        idx_array = idx_array_new.copy()

    return x_array, y_array, t_array

def apply_sqrt_color(t_array, gamma=2):
    t_array_new = t_array / t_array.max()
    t_array_new = np.power(t_array, 1/gamma)
    return t_array_new

def plot_julia(x_array, y_array, t_array):
    t_array_new = apply_sqrt_color(t_array)
    
    fig, ax = plt.subplots()
    fig.set_size_inches(6, 6)
    ax.pcolormesh(x_array, y_array, t_array_new, cmap=plt.cm.get_cmap('gist_earth'))
    ax.set_title("Julia Set c={}".format(settings['c_julia']))
    plt.xlabel("Real axis")
    plt.ylabel("Imaginary axis")
    plt.show()
    
    return fig, ax

def save_figure(fig, folder, dpi, settings):
    filename = "julia_{}_{}_{}.png".format(settings['c_julia'].real, settings['c_julia'].imag, uuid.uuid4())
    filepath = "{}/{}".format(folder, filename)
    fig.savefig(filepath, format="png", dpi=dpi)
    return filepath

print('Start of calculation')
t_start = datetime.datetime.now()

x_array, y_array, t_array = compute_julia(settings)
    
t_end = datetime.datetime.now()
print('Completed !')
print("Calculation duration: {} seconds".format(t_end-t_start))

print('Plot Julia Set figure')
fig, ax = plot_julia(x_array, y_array, t_array)

# Save image to /output
filepath = save_figure(fig=fig, folder="./output", dpi=len(x_array)/6, settings=settings)
print('Save figure to {}'.format(filepath))