#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Modules
import numpy as np
import matplotlib.pyplot as plt
import datetime, uuid

# Global parameters
n_width = 80 # Number of sets in width dim
n_height = 80 # Number of sets in width dim
julia_set_size = 50 # Pizel width/height of each individual Julia set
n_max_iteration = 1000 # Maximum number of iteration of Zn

settings = {
    "n_width": n_width,
    "n_height": n_height,
    "julia_set_size": julia_set_size,
    "n_max_iteration": n_max_iteration
    }

# Functions
def compute_julia(x_array, y_array, c_julia, settings):
    xx, yy = np.meshgrid(x_array, y_array)
    m_array = xx + 1j*yy
    t_array = np.zeros((len(y_array), len(x_array)), dtype = 'float64')
    index_compute = np.indices(m_array.shape)
    idx_array = list(zip(index_compute[0].flatten(), index_compute[1].flatten()))      
    for idx_iteration in range(1, settings['n_max_iteration']):
        idx_array_new = []
        for ii, idx in enumerate(idx_array):
            m_array[idx] = m_array[idx]**2+c_julia
            if np.abs(m_array[idx]) > 2:
                t_array[idx] = idx_iteration + 1
                continue
            idx_array_new.append(idx)
        idx_array = idx_array_new.copy()

    return t_array

def compute_julia_mandelbrot_figure(settings):
    x_array = np.linspace(-1.6, 1.6, settings['julia_set_size'])
    y_array = np.linspace(-1.6, 1.6, settings['julia_set_size'])
    t_array = np.zeros((settings['n_width']*len(y_array), settings['n_height']*len(x_array)))
     
    x_size = len(x_array)
    y_size = len(y_array)
    for idx_julia_x in range(settings['n_width']):
        for idx_julia_y in range(settings['n_height']):
            x_min = -2 + 3*idx_julia_x/settings['n_width']
            x_max = -2 + 3*(idx_julia_x+1)/settings['n_width']
            y_min = -1.5 + 3*idx_julia_y/settings['n_height']
            y_max = -1.5 + 3*(idx_julia_y+1)/settings['n_height']
            c_julia = (x_min+x_max)/2 + 1j*((y_min+y_max)/2)
            t_array[idx_julia_y*y_size:(idx_julia_y+1)*y_size, idx_julia_x*x_size:(idx_julia_x+1)*x_size] = compute_julia(x_array, y_array, c_julia, settings)
        
    x_array = np.linspace(-2, 1, settings['n_width']*settings['julia_set_size'])
    y_array = np.linspace(-1.5, 1.5, settings['n_height']*settings['julia_set_size'])
    return x_array, y_array, t_array

def apply_sqrt_color(t_array, gamma=2):
    t_array_new = t_array / t_array.max()
    t_array_new = np.power(t_array, 1/gamma)
    return t_array_new

def plot_julia_mandelbrot(x_array, y_array, t_array):
    t_array_new = apply_sqrt_color(t_array)
    
    fig, ax = plt.subplots()
    fig.set_size_inches(6, 6)
    ax.pcolormesh(x_array, y_array, t_array_new, cmap=plt.cm.get_cmap('CMRmap'))
    ax.set_title("Mandelbrot shadow from multiple Julia Set")
    plt.xlabel("Real axis")
    plt.ylabel("Imaginary axis")
    plt.show()
    
    return fig, ax

def save_figure(fig, folder, dpi, settings):
    filename = "mandelbrot_julia_{}.png".format(uuid.uuid4())
    filepath = "{}/{}".format(folder, filename)
    fig.savefig(filepath, format="png", dpi=dpi)
    return filepath

print('Start of calculation')
t_start = datetime.datetime.now()

x_array, y_array, t_array = compute_julia_mandelbrot_figure(settings)
    
t_end = datetime.datetime.now()
print('Completed !')
print("Calculation duration: {} seconds".format(t_end-t_start))

print('Plot Manelbrot shadow from Julia Set figure')
fig, ax = plot_julia_mandelbrot(x_array, y_array, t_array)

# Save image to /output
filepath = save_figure(fig=fig, folder="./output", dpi=len(x_array)/6, settings=settings)
print('Save figure to {}'.format(filepath))